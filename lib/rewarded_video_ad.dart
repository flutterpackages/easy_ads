import 'dart:async';

import 'package:firebase_admob/firebase_admob.dart';
import 'package:static_sharedpreferences/static_sharedpreferences.dart';

import 'easy_ads.dart';
import 'assets.dart';
import 'mobile_ad_targeting_info.dart';

class EasyRewardedVideoAd {
  /// ```
  /// Android: 'ca-app-pub-3940256099942544/5224354917'
  ///
  /// IOS: 'ca-app-pub-3940256099942544/1712485313'
  /// ```
  static final String testAdUnitId = RewardedVideoAd.testAdUnitId;

  /// You have to define this with: RewardedVideoAd.instance
  RewardedVideoAd _rewardedVideoAd;

  /// If not given as an Argument, then EasyAds.defaultTargetingInfo will be used
  EasyMobileAdTargetingInfo targetingInfo;

  /// By default the EasyRewardedVideoAd.testAdUnitId is used.
  String adUnitId;

  /// ## Description
  ///
  /// * Default is `true`
  ///
  ///
  /// * If `true` then the Ad will automatically be reloaded on the following events:
  ///
  ///   `RewardedVideoAdEvent.closed`
  ///   `RewardedVideoAdEvent.leftApplication`
  ///   `RewardedVideoAdEvent.failedToLoad`
  bool autoReload;

  /// ## Description
  ///
  /// * Only works if [autoReload] is true.
  ///
  ///
  /// * In case the current event is `RewardedVideoAdEvent.failedToLoad`
  ///   then this Timer will start and when its' callback is called
  ///   [load] gets executed. This prevents the Ad from constantly
  ///   trying to reload in case of an error.
  ///
  ///
  /// * If not changed, then the Timer waits [failedToLoadDuration] before it
  ///   tries to load the Ad again
  Timer get failedToLoadTimer => _failedToLoadTimer;
  Timer _failedToLoadTimer;

  /// ## Description
  ///
  /// * This is user for [failedToLoadTimer].
  ///
  ///
  /// * Initialized with `2 minutes & 30 seconds`.
  Duration failedToLoadDuration;

  /// * Set to `true` on `RewardedVideoAdEvent.loaded`
  ///   and set to `false` on `show`
  ///
  ///
  /// * Initialized wit `false`.
  bool get isLoaded => _isLoaded;
  bool _isLoaded;

  /// * Set to `false` on `RewardedVideoAdEvent.loaded`
  ///   and set to `true` on `RewardedVideoAdEvent.rewarded`
  ///
  ///
  /// * Initialized wit `false`.
  bool get isRewarded => _isRewarded;
  bool _isRewarded;

  /// * Set to `true` on `RewardedVideoAdEvent.completed`
  ///   and set to `false` on `RewardedVideoAdEvent.opened`.
  ///
  ///
  /// * Initialized wit `false`.
  bool get isCompleted => _isCompleted;
  bool _isCompleted;

  /// * Set to `true` on `RewardedVideoAdEvent.closed`
  ///   and set to `false` on `RewardedVideoAdEvent.opened`.
  ///
  ///
  /// * Initialized wit `false`.
  bool get isClosed => _isClosed;
  bool _isClosed;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the AdLoaded event.
  ///
  /// ## Parameters
  ///
  /// * none
  AdLoadedFunction onAdLoaded;

  /// Amount of times the RewardedVideoAdEvent.loaded triggered
  int _loadedCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the AdOpened event.
  ///
  /// ## Parameters
  ///
  /// * none
  AdOpenedFunction onAdOpened;

  /// Amount of times the RewardedVideoAdEvent.opened triggered
  int _openedCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the AdStarted event.
  ///
  /// ## Parameters
  ///
  /// * none
  AdStartedFunction onAdStarted;

  /// Amount of times the RewardedVideoAdEvent.started triggered
  int _startedCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the AdRewarded event.
  ///
  /// ## Parameters
  ///
  /// * int rewardedAmount
  /// * String rewardedType
  AdRewardedFunction onAdRewarded;

  ///Amount of times the RewardedVideoAdEvent.rewarded triggered
  int _rewardedCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the AdCompleted event.
  ///
  /// ## Parameters
  ///
  /// * none
  AdCompletedFunction onAdCompleted;

  /// Amount of times the RewardedVideoAdEvent.completed triggered
  int _completedCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the AdClosed event.
  ///
  /// ## Parameters
  ///
  /// * none
  AdClosedFunction onAdClosed;

  /// Amount of times the RewardedVideoAdEvent.closed triggered
  int _closedCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the LeftApplication event.
  ///
  /// ## Parameters
  ///
  /// * none
  AdLeftApplicationFunction onLeftApplication;

  /// Amount of times the RewardedVideoAdEvent.leftApplication triggered
  int _leftApplicationCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the AdFailedToLoad event.
  ///
  /// ## Parameters
  ///
  /// * none
  AdFailedToLoadFunction onAdFailedToLoad;

  /// Amount of times the RewardedVideoAdEvent.failedToLoad triggered
  int _failedToLoadCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on every event.
  ///
  /// ## Parameters
  ///
  /// * String event
  VideoAdEventTriggeredFunction onAdEvent;

  /// ## Description
  ///
  /// * You `have to` call [EasyAds.initialize], otherwise this class will not work.
  ///
  ///
  /// * The [load] method is called in the constructor.
  ///
  ///
  /// * This Ad will automatically be reloaded.
  ///
  /// ## Example
  ///
  /// {@tool sample}
  /// ```
  /// class MyAdWidget extends StatefulWidget {
  ///   @override
  ///   _MyAdWidgetState createState() => _MyAdWidgetState();
  /// }
  ///
  /// class _MyAdWidgetState extends State<MyAdWidget> {
  ///   EasyRewardedVideoAd myTestAd;
  ///
  ///   @override
  ///   void initState() {
  ///     super.initState();
  ///     myTestAd = EasyRewardedVideoAd();
  ///   }
  ///
  ///   @override
  ///   Widget build(BuildContext context) {
  ///     return Scaffold(
  ///       body: Container(
  ///         child: Center(
  ///           child: RaisedButton(onPressed: myTestAd.show),
  ///         ),
  ///       ),
  ///     );
  ///   }
  /// }
  ///
  /// ```
  /// {@end-tool}
  EasyRewardedVideoAd({
    this.adUnitId,
    this.targetingInfo = EasyAds.defaultTargetingInfo,
    this.autoReload = true,
    bool loadAdInConstructor = true,
    this.failedToLoadDuration = const Duration(minutes: 2, seconds: 30),
    this.onAdLoaded,
    this.onAdOpened,
    this.onAdStarted,
    this.onAdRewarded,
    this.onAdCompleted,
    this.onAdClosed,
    this.onLeftApplication,
    this.onAdFailedToLoad,
    this.onAdEvent,
  }) {
    _rewardedVideoAd = RewardedVideoAd.instance;

    _initValues();
    _initAdListener();

    if (loadAdInConstructor) load();
  }

  ///returns:
  ///```
  ///AdType.VIDEO
  ///```
  AdType get adType => AdType.VIDEO;

  /// If you override this method make sure to call
  /// `super._initValues()` before implementing your code.
  void _initValues() {
    adUnitId = adUnitId ?? EasyRewardedVideoAd.testAdUnitId;

    _isLoaded = false;
    _isRewarded = false;
    _isCompleted = false;
    _isClosed = false;

    _loadedCount = 0;
    _openedCount = 0;
    _startedCount = 0;
    _rewardedCount = 0;
    _completedCount = 0;
    _closedCount = 0;
    _leftApplicationCount = 0;
    _failedToLoadCount = 0;
  }

  void _initAdListener() {
    _rewardedVideoAd.listener =
        (RewardedVideoAdEvent event, {String rewardType, int rewardAmount}) {
      switch (event) {
        case RewardedVideoAdEvent.loaded:
          _loadedCount++;
          _isLoaded = true;
          _isRewarded = false;
          if (onAdLoaded != null) onAdLoaded();
          break;

        case RewardedVideoAdEvent.opened:
          _openedCount++;
          _isClosed = false;
          _isCompleted = false;
          if (onAdOpened != null) onAdOpened();
          break;

        case RewardedVideoAdEvent.started:
          _startedCount++;
          if (onAdStarted != null) onAdStarted();
          break;

        case RewardedVideoAdEvent.rewarded:
          _rewardedCount++;
          _isRewarded = true;
          if (onAdRewarded != null) onAdRewarded(rewardAmount, rewardType);
          break;

        case RewardedVideoAdEvent.completed:
          _completedCount++;
          _isCompleted = true;
          if (onAdCompleted != null) onAdCompleted();
          break;

        case RewardedVideoAdEvent.closed:
          _closedCount++;
          _isClosed = true;
          if (autoReload) load();
          if (onAdClosed != null) onAdClosed();
          break;

        case RewardedVideoAdEvent.leftApplication:
          _leftApplicationCount++;
          if (autoReload) load();
          if (onLeftApplication != null) onLeftApplication();
          break;

        case RewardedVideoAdEvent.failedToLoad:
          _failedToLoadCount++;
          if (autoReload &&
              (_failedToLoadTimer == null || !_failedToLoadTimer.isActive)) {
            _failedToLoadTimer = Timer(failedToLoadDuration, load);
          }
          if (onAdFailedToLoad != null) onAdFailedToLoad();
          break;
      }
      if (onAdEvent != null && event == RewardedVideoAdEvent.rewarded) {
        onAdEvent(
          getEasyRewardedVideoAdEvent(event),
          rewardType: rewardType,
          rewardAmount: rewardAmount,
        );
      } else if (onAdEvent != null) {
        onAdEvent(getEasyRewardedVideoAdEvent(event));
      }
    };
  }

  /// Only shows the RewardedVideoAd if it has been loaded.
  Future<bool> show() {
    if (_isLoaded) {
      _isLoaded = false;
      return _rewardedVideoAd.show();
    }
    return null;
  }

  /// ## Description
  ///
  /// * This method is called in the constructor
  ///
  ///
  /// * Loads Ad, this method is automatically called in the default Listener on the appropriate events.
  Future<bool> load({EasyMobileAdTargetingInfo mobileAdtargetingInfo}) =>
      _rewardedVideoAd.load(
        adUnitId: adUnitId,
        targetingInfo: (mobileAdtargetingInfo != null)
            ? mobileAdtargetingInfo.getMobileAdTargetingInfo()
            : targetingInfo.getMobileAdTargetingInfo(),
      );

  /// Sets the user id to be used in server-to-server reward callbacks.
  void setUserId(String userId) {
    _rewardedVideoAd.userId = userId;
  }

  /// Sets custom data to be included in server-to-server reward callbacks.
  void setCustomData(String customData) {
    _rewardedVideoAd.customData = customData;
  }

  ///The user id used in server-to-server reward callbacks
  String get userId => _rewardedVideoAd.userId;

  /// Get the RewardedVideoAd instance from the `firebase_admob` library
  RewardedVideoAd get rewardedVideoAd => _rewardedVideoAd;

  /// * Amount of times the RewardedVideoAdEvent.loaded triggered.
  ///
  ///
  /// * Before `onAdLoaded` is called `loadedCount++` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get loadedCount => _loadedCount;

  /// * Amount of times the RewardedVideoAdEvent.opened triggered.
  ///
  ///
  /// * Before `onAdOpened` is called `openedCount` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get openedCount => _openedCount;

  /// * Amount of times the RewardedVideoAdEvent.started triggered.
  ///
  ///
  /// * Before `onAdStarted` is called `startedCount` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get startedCount => _startedCount;

  /// * Amount of times the RewardedVideoAdEvent.rewarded triggered.
  ///
  ///
  /// * Before `onAdRewarded` is called `rewardedCount` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get rewardedCount => _rewardedCount;

  /// * Amount of times the RewardedVideoAdEvent.completed triggered.
  ///
  ///
  /// * Before `onAdCompleted` is called `completedCount` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get completedCount => _completedCount;

  /// * Amount of times the RewardedVideoAdEvent.closed triggered.
  ///
  ///
  /// * Before `onAdClosed` is called `closedCount` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get closedCount => _closedCount;

  /// * Amount of times the RewardedVideoAdEvent.leftApplication triggered.
  ///
  ///
  /// * Before `onLeftApplication` is called `leftApplicationCount` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get leftApplicationCount => _leftApplicationCount;

  /// * Amount of times the RewardedVideoAdEvent.failedToLoad triggered.
  ///
  ///
  /// * Before `onAdFailedToLoad` is called `failedToLoadCount++` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get failedToLoadCount => _failedToLoadCount;
}

class HealthyEasyRewardedVideoAd extends EasyRewardedVideoAd {
  int get blockAdsUntilCount => _blockAdsUntilCount;
  int _blockAdsUntilCount;
  final String _blockAdsUntilCountKey =
      EasyAds.SP_KEY + AdType.HEALTHY_VIDEO.toString() + "_blockAdsUntilCount";

  /// ## Description
  ///
  /// * Amount of times [show] has to
  ///   be executed until the `RewardedVideoAd` will really be shown.
  ///
  ///
  /// * Default value is `50`.
  ///
  ///
  /// * Can only be changed in the constructor.
  ///
  ///
  /// * Has to be `>= 0`, or else it will be initialized with `50`.
  int get blockAdsUntil => _blockAdsUntil;
  int _blockAdsUntil;

  /// ## Description
  ///
  /// * If you are using [showOnCount], then this
  ///   returns the amount of times the user has already triggered
  ///   [showOnCount].
  int get count => _count;
  int _count;
  final String _countKey =
      EasyAds.SP_KEY + AdType.HEALTHY_VIDEO.toString() + "_countKey";

  /// ## Description
  ///
  /// * If you are using [showOnCountAffected], then this
  ///   returns the amount of times the user has already triggered
  ///   [showOnCountAffected].
  int _countAffected;
  final String _countAffectedKey =
      EasyAds.SP_KEY + AdType.HEALTHY_VIDEO.toString() + "_countAffectedKey";

  /// ## Description
  ///
  /// * You `have to initialize` [EasyAds.initialize], otherwise this class will not work.
  ///
  ///
  /// * For more information regarding the parameters see [EasyRewardedVideoAd].
  ///
  ///
  /// * This class helps you not to flood your users with ads.
  ///
  ///
  /// ## Example
  ///
  /// {@tool sample}
  /// ```dart
  /// void main(){
  ///   WidgetsFlutterBinding.ensureInitialized();
  ///
  ///   EasyAds.initialize();
  ///
  ///   EasyAds.initEasyRewardedVideoAd(
  ///     HealthyEasyRewardedVideoAd(
  ///       blockAdsUntil: 5,
  ///     ),
  ///   );
  ///
  ///   runApp(MyApp());
  /// }
  ///
  /// class MyApp extends StatelessWidget {
  ///   @override
  ///   Widget build(BuildContext context) {
  ///     return MaterialApp(
  ///       title: 'EasyAds Demo',
  ///       theme: ThemeData(
  ///         primarySwatch: Colors.blue,
  ///       ),
  ///       home: MyAdWidget(),
  ///     );
  ///   }
  /// }
  ///
  /// class MyAdWidget extends StatefulWidget {
  ///   @override
  ///   _MyAdWidgetState createState() => _MyAdWidgetState();
  /// }
  ///
  /// class _MyAdWidgetState extends State<MyAdWidget> {
  ///
  ///   @override
  ///   Widget build(BuildContext context) {
  ///     return Scaffold(
  ///       body: Container(
  ///         child: Center(
  ///           child: RaisedButton(
  ///             // Because we set blockAdsUntil = 5 in the
  ///             // HealthyEasyRewardedVideoAd constructor
  ///             // the ad will not be shown until the
  ///             // user has clicked the button at least 5 times
  ///             onPressed: EasyAds.showEasyRewardedVideoAd,
  ///           ),
  ///         ),
  ///       ),
  ///     );
  ///   }
  /// }
  /// ```
  /// {@end-tool}
  HealthyEasyRewardedVideoAd({
    int blockAdsUntil = 50,
    String adUnitId,
    EasyMobileAdTargetingInfo targetingInfo = EasyAds.defaultTargetingInfo,
    bool autoReload = true,
    bool loadAdInConstructor = true,
    Duration failedToLoadDuration = const Duration(minutes: 2, seconds: 30),
    AdLoadedFunction onAdLoaded,
    AdOpenedFunction onAdOpened,
    AdStartedFunction onAdStarted,
    AdRewardedFunction onAdRewarded,
    AdCompletedFunction onAdCompleted,
    AdClosedFunction onAdClosed,
    AdLeftApplicationFunction onLeftApplication,
    AdFailedToLoadFunction onAdFailedToLoad,
    VideoAdEventTriggeredFunction onAdEvent,
  }) : super(
          targetingInfo: targetingInfo,
          adUnitId: adUnitId,
          autoReload: autoReload,
          loadAdInConstructor: loadAdInConstructor,
          failedToLoadDuration: failedToLoadDuration,
          onAdLoaded: onAdLoaded,
          onAdOpened: onAdOpened,
          onAdStarted: onAdStarted,
          onAdRewarded: onAdRewarded,
          onAdCompleted: onAdCompleted,
          onAdClosed: onAdClosed,
          onLeftApplication: onLeftApplication,
          onAdFailedToLoad: onAdFailedToLoad,
          onAdEvent: onAdEvent,
        ) {
    _blockAdsUntil = (blockAdsUntil >= 0) ? blockAdsUntil : 50;
  }

  ///returns:
  ///```
  ///AdType.HEALTHY_VIDEO
  ///```
  @override
  AdType get adType => AdType.HEALTHY_VIDEO;

  @override
  void _initValues() {
    super._initValues();
    SP.execute(_restoreValues);
  }

  void _saveValues() {
    //_blockAdsUntilCount
    SP.setInt(_blockAdsUntilCountKey, _blockAdsUntilCount);

    //_count
    SP.setInt(_countKey, _count);

    //_countAffected
    SP.setInt(_countAffectedKey, _countAffected);
  }

  void _restoreValues() {
    //_blockAdsUntilCount
    _blockAdsUntilCount = SP.getInt(_blockAdsUntilCountKey) ?? 0;

    //_count
    _count = SP.getInt(_countKey) ?? 0;

    //_countAffected
    _countAffected = SP.getInt(_countAffectedKey) ?? 0;
  }

  bool _isAdsUnlocked() {
    return (_blockAdsUntil == 0 || _blockAdsUntil - 1 == _blockAdsUntilCount)
        ? true
        : false;
  }

  bool closedBeforeCompleted() => (super._isClosed && !super._isCompleted);

  /// Only shows the RewardedVideoAd if it has been loaded
  /// and blockAdsUntil has been reached.
  @override
  Future<bool> show() {
    if (_isAdsUnlocked()) {
      return super.show();
    } else {
      _blockAdsUntilCount++;
      _saveValues();
      return null;
    }
  }

  /// ## Description
  ///
  /// * This is not affected by `blockAdsUntil`
  ///
  ///
  /// * The Example assumes, that you have already initialized the
  ///   plugin with [EasyAds.initialize].
  ///
  ///
  /// ## Parameters
  ///
  /// * `onValue`:
  ///
  ///   Amount of times [showOnCount] has to be executed until [show] is called.
  ///
  ///
  /// * `onShowAd`:
  ///
  ///   This function gets executed right before the [EasyRewardedVideoAd.show] function.
  ///   If onShowAd returns `false`, then the ad does not get shown.
  ///
  ///
  /// * `onNotShowAd`:
  ///
  ///   In case the expression that evaluates if an ad will be shown,
  ///   using onValue and a private static counter variable equals false,
  ///   then this function gets executed.
  ///
  ///
  /// * `showAdOnValue`:
  ///
  ///   In case you do not want this function to automatically show an ad
  ///   if the expression that evaluates if an ad will be shown equals true,
  ///   then set this to `false`.
  ///
  ///
  /// * `forceShowIfClosedBeforeCompleted`:
  ///
  ///   If the user closed the video Ad before it was completed,
  ///   then the next time [showOnCount] is called the ad will be
  ///   shown immediately without checking if, for example `onValue = 5`.
  ///
  /// ## Example
  ///
  /// {@tool sample}
  /// ```dart
  /// class MyAdWidget extends StatelessWidget {
  ///   @override
  ///   Widget build(BuildContext context) {
  ///     return Scaffold(
  ///       body: Container(
  ///         child: Center(
  ///           child: RaisedButton(
  ///             onPressed: () {
  ///               // Because we set onValue: 3,
  ///               // every third button press will show an ad,
  ///               // but before the ad is shown onShowAd
  ///               // will get executed if it is non-null.
  ///               //
  ///               // In case the user pressed the button for the first or
  ///               // second time, onNotShowAd will get executed
  ///               // if non-null.
  ///               EasyAds.easyRewardedVideoAd.showOnCount(
  ///                 onValue: 3,
  ///                 onShowAd: () {
  ///                   print("now showing ad");
  ///                   return true;
  ///                 },
  ///                 onNotShowAd: () {
  ///                   print("u r lucky, no ad");
  ///                 },
  ///               );
  ///             },
  ///           ),
  ///         ),
  ///       ),
  ///     );
  ///   }
  /// }
  /// ```
  /// {@end-tool}
  void showOnCount({
    int onValue = 5,
    ShowAdCallback onShowAd,
    NotShowAdCallback onNotShowAd,
    bool showAdOnValue = true,
    bool forceShowIfClosedBeforeCompleted = false,
  }) async {
    if (forceShowIfClosedBeforeCompleted && closedBeforeCompleted()) {
      _count = 0;
      _saveValues();
      if (onShowAd != null && !await onShowAd()) return;
      if (showAdOnValue) super.show();
      return;
    }

    if (_count >= onValue) {
      _count = 0;
      _saveValues();
      if (onShowAd != null && !await onShowAd()) return;
      if (showAdOnValue) super.show();
    } else {
      _count++;
      _saveValues();
      if (onNotShowAd != null) onNotShowAd();
    }
  }

  /// ## Description
  ///
  /// * This is affected by `blockAdsUntil`
  ///
  ///
  /// * The Example assumes, that you have already initialized the
  ///   plugin with [EasyAds.initialize].
  ///
  ///
  /// ## Parameters
  ///
  /// * `onValue`:
  ///
  ///   Amount of times [showOnCountAffected] has to be executed until [show] is called.
  ///
  ///
  /// * `onShowAd`:
  ///
  ///   This function gets executed right before the [EasyRewardedVideoAd.show] function.
  ///   If onShowAd returns `false`, then the ad does not get shown.
  ///
  ///
  /// * `onNotShowAd`:
  ///
  ///   In case the expression that evaluates if an ad will be shown,
  ///   using onValue and a private static counter variable equals false,
  ///   then this function gets executed.
  ///
  ///
  /// * `showAdOnValue`:
  ///
  ///   In case you do not want this function to automatically show an ad
  ///   if the expression that evaluates if an ad will be shown equals true,
  ///   then set this to `false`.
  ///
  ///
  /// * `forceShowIfClosedBeforeCompleted`:
  ///
  ///   If the user closed the video Ad before it was completed,
  ///   then the next time [showOnCount] is called the ad will be
  ///   shown immediately without checking if, for example `onValue = 5`.
  ///
  ///
  /// ## Example
  ///
  /// {@tool sample}
  /// ```dart
  /// class MyAdWidget extends StatelessWidget {
  ///   @override
  ///   Widget build(BuildContext context) {
  ///     return Scaffold(
  ///       body: Container(
  ///         child: Center(
  ///           child: RaisedButton(
  ///             onPressed: () {
  ///               // Because we set onValue: 3,
  ///               // every third button press will show an ad,
  ///               // but before the ad is shown onShowAd
  ///               // will get executed if it is non-null.
  ///               //
  ///               // In case the user pressed the button for the first or
  ///               // second time, onNotShowAd will get executed
  ///               // if non-null.
  ///               EasyAds.easyRewardedVideoAd.showOnCount(
  ///                 onValue: 3,
  ///                 onShowAd: () {
  ///                   print("now showing ad");
  ///                   return true;
  ///                 },
  ///                 onNotShowAd: () {
  ///                   print("u r lucky, no ad");
  ///                 },
  ///               );
  ///             },
  ///           ),
  ///         ),
  ///       ),
  ///     );
  ///   }
  /// }
  /// ```
  /// {@end-tool}
  void showOnCountAffected({
    int onValue = 5,
    ShowAdCallback onShowAd,
    NotShowAdCallback onNotShowAd,
    bool showAdOnValue = true,
    bool forceShowIfClosedBeforeCompleted = false,
  }) async {
    if (forceShowIfClosedBeforeCompleted && closedBeforeCompleted()) {
      _countAffected = 0;
      _saveValues();
      if (onShowAd != null && !await onShowAd()) return;
      if (showAdOnValue) show();
      return null;
    }

    if (_countAffected >= onValue) {
      _countAffected = 0;
      _saveValues();
      if (onShowAd != null && !await onShowAd()) return;
      if (showAdOnValue) show();
    } else {
      _countAffected++;
      _saveValues();
      if (onNotShowAd != null) onNotShowAd();
    }
  }
}
