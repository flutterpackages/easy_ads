import 'package:firebase_admob/firebase_admob.dart';

/// Targeting info per the native AdMob API.
///
/// This class's properties mirror the native AdRequest API. See for example:
/// [AdRequest.Builder for Android](https://firebase.google.com/docs/reference/android/com/google/android/gms/ads/AdRequest.Builder).
class EasyMobileAdTargetingInfo {
  final List<String> keywords;
  final String contentUrl;
  @Deprecated('This functionality is deprecated in AdMob without replacement.')
  final DateTime birthday;
  @Deprecated('This functionality is deprecated in AdMob without replacement.')
  final MobileAdGender gender;
  @Deprecated(
      'This functionality is deprecated in AdMob.  Use `childDirected` instead.')
  final bool designedForFamilies;
  final bool childDirected;
  final List<String> testDevices;
  final bool nonPersonalizedAds;

  const EasyMobileAdTargetingInfo(
      {this.keywords,
      this.contentUrl,
      @Deprecated('This functionality is deprecated in AdMob without replacement.')
          this.birthday,
      @Deprecated('This functionality is deprecated in AdMob without replacement.')
          this.gender,
      @Deprecated('Use `childDirected` instead.')
          this.designedForFamilies,
      this.childDirected,
      this.testDevices,
      this.nonPersonalizedAds});

  /// This converts [EasyMobileAdTargetingInfo] to [MobileAdTargetingInfo]
  /// from the firebase_admob package.
  MobileAdTargetingInfo getMobileAdTargetingInfo() {
    return MobileAdTargetingInfo(
      keywords: keywords,
      contentUrl: contentUrl,
      birthday: birthday,
      gender: gender,
      designedForFamilies: designedForFamilies,
      childDirected: childDirected,
      testDevices: testDevices,
      nonPersonalizedAds: nonPersonalizedAds,
    );
  }
}
