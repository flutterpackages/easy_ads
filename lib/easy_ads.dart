library easy_ads;

import 'package:firebase_admob/firebase_admob.dart';
import 'package:static_sharedpreferences/static_sharedpreferences.dart';

import 'assets.dart';
export 'assets.dart';

import 'mobile_ad_targeting_info.dart';
export 'mobile_ad_targeting_info.dart';

import 'banner_ad.dart';
import 'interstital_ad.dart';
import 'rewarded_video_ad.dart';

/// ## Description
///
/// * Support for Google AdMob mobile ads.
///
///
/// * Before initializing, loading or showing an ad the plugin `must` be initialized
///   {@tool sample}
///   ```
///   void main(){
///     WidgetsFlutterBinding.ensureInitialized();
///
///     EasyAds.initialize();
///
///     EasyAds.initEasyBannerAd(myBannerAd);
///
///     runApp(MyApp);
///   }
///   ```
///  {@end-tool}
///
/// ## See also:
///
///  * [EasyBannerAd], a small rectangular ad displayed at the bottom of the screen.
///  * [EasyInterstitialAd], a full screen ad that must be dismissed by the user.
///  * [EasyRewardedVideoAd], a full screen video ad that provides in-app user
///    rewards.
abstract class EasyAds {
  /// Part of the key, which will be used for
  /// all the SharedPreferences variables in
  /// the EasyAds package
  static const String SP_KEY = "EasyAds";

  static bool _isInitializedFirebaseAdMob = false;
  static bool get isInitialized => _isInitializedFirebaseAdMob;

  /// In case you do not give your Admob ID
  /// as a parameter, then the platform specific
  /// testAppId will be used:
  ///
  /// ```
  /// Android: 'ca-app-pub-3940256099942544~3347511713'
  ///
  /// IOS: 'ca-app-pub-3940256099942544~1458002511'
  /// ```
  static void initialize({
    String admobId,
    String trackingId,
    bool analyticsEnabled = false,
  }) {
    SP.initialize();

    admobId = admobId ?? FirebaseAdMob.testAppId;
    FirebaseAdMob.instance
        .initialize(
      appId: admobId,
      trackingId: trackingId,
      analyticsEnabled: analyticsEnabled,
    )
        .then((onValue) {
      _isInitializedFirebaseAdMob = true;
    });
  }

  static const EasyMobileAdTargetingInfo defaultTargetingInfo =
      EasyMobileAdTargetingInfo(childDirected: false);

  static void setOnSharedPreferencesIsNull(
          Function onSharedPreferencesIsNull) =>
      SP.onSharedPreferencesIsNull = onSharedPreferencesIsNull;

  ///EasyBannerAd ----------------------------------------------------------

  static EasyBannerAd _bannerAd;

  static EasyBannerAd get easyBannerAd => _bannerAd;

  /// Returns the EasyBannerAd typecasted as a HealthyEasyBannerAd,
  /// you should use this if you put a HealthyEasyBannerAd
  /// into `initEasyBannerAd`.
  static HealthyEasyBannerAd get healthyEasyBannerAd =>
      _bannerAd as HealthyEasyBannerAd;

  static void initEasyBannerAd(EasyBannerAd bannerAd) {
    _bannerAd = bannerAd;
  }

  static Future<bool> loadEasyBannerAd() => _bannerAd.load();

  static Future<bool> showEasyBannerAd({
    double anchorOffset = 0.0,
    double horizontalCenterOffset = 0.0,
    EasyAnchorType anchorType = EasyAnchorType.bottom,
  }) =>
      _bannerAd.show(
        anchorOffset: anchorOffset,
        horizontalCenterOffset: horizontalCenterOffset,
        anchorType: anchorType,
      );

  static Future<bool> disposeEasyBannerAd() => _bannerAd.dispose();

  ///EasyInterstitialAd ----------------------------------------------------

  static EasyInterstitialAd _interstitialAd;

  static EasyInterstitialAd get easyInterstitialAd => _interstitialAd;

  /// Returns the EasyInterstitialAd typecasted as a HealthyEasyInterstitialAd,
  /// you should use this if you put a HealthyEasyInterstitialAd
  /// into `initEasyInterstitialAd`.
  static HealthyEasyInterstitialAd get healthyEasyInterstitialAd =>
      _interstitialAd as HealthyEasyInterstitialAd;

  static void initEasyInterstitialAd(EasyInterstitialAd interstitialAd) {
    _interstitialAd = interstitialAd;
  }

  static Future<bool> loadEasyInterstitialAd() => _interstitialAd.load();

  static Future<bool> showEasyInterstitialAd({
    double anchorOffset = 0.0,
    double horizontalCenterOffset = 0.0,
    EasyAnchorType anchorType = EasyAnchorType.bottom,
  }) =>
      _interstitialAd.show(
        anchorOffset: anchorOffset,
        horizontalCenterOffset: horizontalCenterOffset,
        anchorType: anchorType,
      );

  static Future<bool> disposeEasyInterstitialAd() => _interstitialAd.dispose();

  ///EasyRewardedVideoAd ---------------------------------------------------

  static EasyRewardedVideoAd _videoAd;

  static EasyRewardedVideoAd get easyRewardedVideoAd => _videoAd;

  /// Returns the EasyRewardedVideoAd typecasted as a HealthyEasyRewardedVideoAd,
  /// you should use this if you put a HealthyEasyRewardedVideoAd
  /// into `initEasyRewardedVideoAd`.
  static HealthyEasyRewardedVideoAd get healthyEasyRewardedVideoAd =>
      _videoAd as HealthyEasyRewardedVideoAd;

  static void initEasyRewardedVideoAd(EasyRewardedVideoAd videoAd) {
    _videoAd = videoAd;
  }

  static Future<bool> loadEasyRewardedVideoAd(
          {EasyMobileAdTargetingInfo targetingInfo}) =>
      (targetingInfo != null)
          ? _videoAd.load(mobileAdtargetingInfo: targetingInfo)
          : _videoAd.load();

  static Future<bool> showEasyRewardedVideoAd() => _videoAd.show();
}
