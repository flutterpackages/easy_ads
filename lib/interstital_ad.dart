import 'dart:async';

import 'package:firebase_admob/firebase_admob.dart';
import 'package:static_sharedpreferences/static_sharedpreferences.dart';

import 'easy_ads.dart';
import 'assets.dart';
import 'mobile_ad_targeting_info.dart';

class EasyInterstitialAd {
  /// ```
  /// Android: 'ca-app-pub-3940256099942544/1033173712'
  ///
  /// IOS: 'ca-app-pub-3940256099942544/4411468910'
  /// ```
  static final String testAdUnitId = InterstitialAd.testAdUnitId;

  InterstitialAd _interstitialAd;

  MobileAdListener _listener;

  /// If not given as an Argument, then EasyAds.defaultTargetingInfo will be used
  EasyMobileAdTargetingInfo targetingInfo;

  /// By default the EasyInterstitialAd.testAdUnitId is used.
  String adUnitId;

  /// ## Description
  ///
  /// * Default is `true`
  ///
  ///
  /// * If `true` then the Ad will automatically be reloaded on the following events:
  ///
  ///   `MobileAdEvent.closed`
  ///   `MobileAdEvent.leftApplication`
  ///   `MobileAdEvent.failedToLoad`
  bool autoReload;

  /// ## Description
  ///
  /// * Only works if [autoReload] is true.
  ///
  ///
  /// * In case the current event is `MobileAdEvent.failedToLoad`
  ///   then this Timer will start and when its' callback is called
  ///   [load] gets executed. This prevents the Ad from constantly
  ///   trying to reload in case of an error.
  ///
  ///
  /// * If not changed, then the Timer waits [failedToLoadDuration] before it
  ///   tries to load the Ad again
  Timer get failedToLoadTimer => _failedToLoadTimer;
  Timer _failedToLoadTimer;

  /// ## Description
  ///
  /// * This is user for [failedToLoadTimer].
  ///
  ///
  /// * Initialized with `2 minutes & 30 seconds`.
  Duration failedToLoadDuration;

  Future<bool> isLoaded() => _interstitialAd.isLoaded();

  /// * Set to `true` on `MobileAdEvent.closed`
  ///   and set to `false` on `MobileAdEvent.opened`.
  ///
  ///
  /// * Initialized wit `false`.
  bool get isClosed => _isClosed;
  bool _isClosed;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the AdLoaded event.
  ///
  /// ## Parameters
  ///
  /// * none
  AdLoadedFunction onAdLoaded;

  /// Amount of times the MobileAdEvent.loaded triggered
  int _loadedCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the AdClicked event.
  ///
  /// ## Parameters
  ///
  /// * none
  AdClickedFunction onAdClicked;

  /// Amount of times the MobileAdEvent.loaded triggered
  int _clickedCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the AdOpened event.
  ///
  /// ## Parameters
  ///
  /// * none
  AdOpenedFunction onAdOpened;

  /// Amount of times the MobileAdEvent.opened triggered
  int _openedCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the AdImpression event.
  ///
  /// ## Parameters
  ///
  /// * none
  AdStartedFunction onAdImpression;

  /// Amount of times the MobileAdEvent.impression triggered
  int _impressionCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the AdClosed event.
  ///
  /// ## Parameters
  ///
  /// * none
  AdClosedFunction onAdClosed;

  /// Amount of times the MobileAdEvent.closed triggered,
  int _closedCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the LeftApplication event.
  ///
  /// ## Parameters
  ///
  /// * none
  AdLeftApplicationFunction onLeftApplication;

  /// Amount of times the MobileAdEvent.leftApplication triggered
  int _leftApplicationCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on the AdFailedToLoad event.
  ///
  /// ## Parameters
  ///
  /// * none
  AdFailedToLoadFunction onAdFailedToLoad;

  /// Amount of times the MobileAdEvent.failedToLoad triggered
  int _failedToLoadCount;

  /// ## Description
  ///
  /// * Fuction for the default Listener, on every event.
  ///
  /// ## Parameters
  ///
  /// * String event
  MobileAdEventTriggeredFunction onAdEvent;

  /// ## Description
  ///
  /// * You `have to` call [EasyAds.initialize], otherwise this class will not work.
  ///
  ///
  /// * The [load] method is called in the constructor.
  ///
  /// ## Example
  ///
  /// {@tool sample}
  /// ```
  /// class MyAdWidget extends StatefulWidget {
  ///   @override
  ///   _MyAdWidgetState createState() => _MyAdWidgetState();
  /// }
  ///
  /// class _MyAdWidgetState extends State<MyAdWidget> {
  ///   EasyInterstitialAd myTestAd;
  ///
  ///   @override
  ///   void initState() {
  ///     super.initState();
  ///     myTestAd = EasyInterstitialAd();
  ///   }
  ///
  ///   @override
  ///   Widget build(BuildContext context) {
  ///     return Scaffold(
  ///       body: Container(
  ///         child: Center(
  ///           child: RaisedButton(onPressed: myTestAd.show),
  ///         ),
  ///       ),
  ///     );
  ///   }
  /// }
  ///
  /// ```
  /// {@end-tool}
  EasyInterstitialAd({
    this.adUnitId,
    this.targetingInfo = EasyAds.defaultTargetingInfo,
    this.autoReload = true,
    bool loadAdInConstructor = true,
    this.failedToLoadDuration = const Duration(minutes: 2, seconds: 30),
    this.onAdLoaded,
    this.onAdClicked,
    this.onAdOpened,
    this.onAdImpression,
    this.onAdClosed,
    this.onLeftApplication,
    this.onAdFailedToLoad,
    this.onAdEvent,
  }) {
    _initValues();
    _initAdListener();

    _interstitialAd = InterstitialAd(
      adUnitId: adUnitId,
      targetingInfo: targetingInfo.getMobileAdTargetingInfo(),
      listener: _listener,
    );

    if (loadAdInConstructor) load();
  }

  ///returns:
  ///```
  ///AdType.INTERSTITIAL
  ///```
  AdType get adType => AdType.INTERSTITIAL;

  void _initValues() {
    adUnitId = adUnitId ?? EasyInterstitialAd.testAdUnitId;

    _isClosed = false;

    _loadedCount = 0;
    _openedCount = 0;
    _impressionCount = 0;
    _closedCount = 0;
    _leftApplicationCount = 0;
    _failedToLoadCount = 0;
  }

  void _initAdListener() {
    _listener = (MobileAdEvent event) {
      switch (event) {
        case MobileAdEvent.loaded:
          _loadedCount++;
          if (onAdLoaded != null) onAdLoaded();
          break;

        case MobileAdEvent.clicked:
          _clickedCount++;
          if (onAdClicked != null) onAdClicked();
          break;

        case MobileAdEvent.opened:
          _openedCount++;
          _isClosed = false;
          if (onAdOpened != null) onAdOpened();
          break;

        case MobileAdEvent.closed:
          _closedCount++;
          _isClosed = true;
          if (autoReload) load();
          if (onAdClosed != null) onAdClosed();
          break;

        case MobileAdEvent.impression:
          _impressionCount++;
          if (onAdImpression != null) onAdImpression();
          break;

        case MobileAdEvent.leftApplication:
          _leftApplicationCount++;
          if (autoReload) load();
          if (onLeftApplication != null) onLeftApplication();
          break;

        case MobileAdEvent.failedToLoad:
          _failedToLoadCount++;
          if (autoReload &&
              (_failedToLoadTimer == null || !_failedToLoadTimer.isActive)) {
            _failedToLoadTimer = Timer(failedToLoadDuration, load);
          }
          if (onAdFailedToLoad != null) onAdFailedToLoad();
          break;
      }
      if (onAdEvent != null) onAdEvent(getEasyMobileAdEvent(event));
    };
  }

  Future<bool> load() => _interstitialAd.load();

  /// ## Description
  ///
  /// * The ad must have been loaded with `load` first.
  ///   If loading hasn't finished the ad will not actually
  ///   appear until the ad has finished loading.
  ///
  ///
  /// * The event specific method will get called when the ad has finished
  ///   loading or fails to do so e.g.:
  ///   ```
  ///   MobileAdEvent.loaded => onAdLoaded()
  ///   ```
  ///
  ///
  /// * `anchorOffset` is the logical pixel offset from the edge
  ///   of the screen (default 0.0). `anchorType` place advert at
  ///   top or bottom of screen (default bottom).
  Future<bool> show({
    double anchorOffset = 0.0,
    double horizontalCenterOffset = 0.0,
    EasyAnchorType anchorType = EasyAnchorType.bottom,
  }) =>
      _interstitialAd.show(
          anchorOffset: anchorOffset,
          horizontalCenterOffset: horizontalCenterOffset,
          anchorType: getAnchorType(anchorType));

  /// ## Description
  ///
  /// * Put this in `dispose` or `deactivate`, if you are using
  ///   a Stateful Widget
  ///
  ///
  /// * Free the plugin resources associated with this ad.
  ///
  ///
  /// * Disposing a banner ad that's been shown removes it from the screen.
  ///   Interstitial ads can't be programmatically removed from view.
  Future<bool> dispose() => _interstitialAd.dispose();

  /// Get the InterstitialAd instance from the `firebase_admob` library
  InterstitialAd get interstitialAd => _interstitialAd;

  /// * Amount of times the MobileAdEvent.loaded triggered.
  ///
  ///
  /// * Before `onAdLoaded` is called `loadedCount` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get loadedCount => _loadedCount;

  /// * Amount of times the MobileAdEvent.clicked triggered.
  ///
  ///
  /// * Before `onAdClicked` is called `clickedCount` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get clickedCount => _clickedCount;

  /// * Amount of times the MobileAdEvent.opened triggered.
  ///
  ///
  /// * Before `onAdOpened` is called `openedCount` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get openedCount => _openedCount;

  /// * Amount of times the MobileAdEvent.impression triggered.
  ///
  ///
  /// * Before `onAdImpression` is called `impressionCount` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get impressionCount => _impressionCount;

  /// * Amount of times the MobileAdEvent.closed triggered.
  ///
  ///
  /// * Before `onAdClosed` is called `closedCount` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get closedCount => _closedCount;

  /// * Amount of times the MobileAdEvent.leftApplication triggered.
  ///
  ///
  /// * Before `onLeftApplication` is called `leftApplicationCount` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get leftApplicationCount => _leftApplicationCount;

  /// * Amount of times the MobileAdEvent.failedToLoad triggered.
  ///
  ///
  /// * Before `onAdFailedToLoad` is called `failedToLoadCount` gets executed.
  ///
  ///
  /// * This value will not be saved in any form.
  int get failedToLoadCount => _failedToLoadCount;
}

class HealthyEasyInterstitialAd extends EasyInterstitialAd {
  int get blockAdsUntilCount => _blockAdsUntilCount;
  int _blockAdsUntilCount;
  final String _blockAdsUntilCountKey = EasyAds.SP_KEY +
      AdType.HEALTHY_INTERSTITIAL.toString() +
      "_blockAdsUntilCount";

  /// ## Description
  ///
  /// * Can only be changed in the constructor.
  ///
  ///
  /// * Amount of times a certain event has to
  ///   be executed until the `InterstitialAd` will be shown
  ///
  ///
  /// * This will be initialized with `50`
  int get blockAdsUntil => _blockAdsUntil;
  int _blockAdsUntil;

  /// ## Description
  ///
  /// * If you are using [showOnCount], then this
  ///   returns the amount of times the user has already triggered
  ///   [showOnCount].
  int get count => _count;
  int _count;
  final String _countKey =
      EasyAds.SP_KEY + AdType.HEALTHY_INTERSTITIAL.toString() + "_countKey";

  /// ## Description
  ///
  /// * If you are using [showOnCountAffected], then this
  ///   returns the amount of times the user has already triggered
  ///   [showOnCountAffected].
  int _countAffected;
  final String _countAffectedKey = EasyAds.SP_KEY +
      AdType.HEALTHY_INTERSTITIAL.toString() +
      "_countAffectedKey";

  /// ## Description
  ///
  /// * You `have to initialize` [EasyAds.initialize], otherwise this class will not work.
  ///
  ///
  /// * For more information regarding the parameters see [EasyInterstitialAd].
  ///
  ///
  /// * This class helps you not to flood your users with ads.
  ///
  ///
  /// ## Example
  ///
  /// {@tool sample}
  /// ```dart
  /// void main(){
  ///   WidgetsFlutterBinding.ensureInitialized();
  ///
  ///   EasyAds.initialize();
  ///
  ///   EasyAds.initEasyInterstitialAd(
  ///     HealthyEasyInterstitialAd(
  ///       blockAdsUntil: 5,
  ///     ),
  ///   );
  ///
  ///   runApp(MyApp());
  /// }
  ///
  /// class MyApp extends StatelessWidget {
  ///   @override
  ///   Widget build(BuildContext context) {
  ///     return MaterialApp(
  ///       title: 'EasyAds Demo',
  ///       theme: ThemeData(
  ///         primarySwatch: Colors.blue,
  ///       ),
  ///       home: MyAdWidget(),
  ///     );
  ///   }
  /// }
  ///
  /// class MyAdWidget extends StatefulWidget {
  ///   @override
  ///   _MyAdWidgetState createState() => _MyAdWidgetState();
  /// }
  ///
  /// class _MyAdWidgetState extends State<MyAdWidget> {
  ///
  ///   @override
  ///   Widget build(BuildContext context) {
  ///     return Scaffold(
  ///       body: Container(
  ///         child: Center(
  ///           child: RaisedButton(
  ///             // Because we set blockAdsUntil = 5 in the
  ///             // HealthyEasyInterstitialAd constructor
  ///             // the ad will not be shown until the
  ///             // user has clicked the button at least 5 times
  ///             onPressed: EasyAds.showEasyInterstitialAd,
  ///           ),
  ///         ),
  ///       ),
  ///     );
  ///   }
  /// }
  /// ```
  /// {@end-tool}
  HealthyEasyInterstitialAd({
    int blockAdsUntil = 50,
    String adUnitId,
    EasyMobileAdTargetingInfo targetingInfo = EasyAds.defaultTargetingInfo,
    bool autoReload = true,
    bool loadAdInConstructor = true,
    Duration failedToLoadDuration = const Duration(minutes: 2, seconds: 30),
    AdLoadedFunction onAdLoaded,
    AdClickedFunction onAdClicked,
    AdOpenedFunction onAdOpened,
    AdImpressionFunction onAdImpression,
    AdClosedFunction onAdClosed,
    AdLeftApplicationFunction onLeftApplication,
    AdFailedToLoadFunction onAdFailedToLoad,
    MobileAdEventTriggeredFunction onAdEvent,
  }) : super(
          adUnitId: adUnitId,
          targetingInfo: targetingInfo,
          autoReload: autoReload,
          loadAdInConstructor: loadAdInConstructor,
          failedToLoadDuration: failedToLoadDuration,
          onAdLoaded: onAdLoaded,
          onAdClicked: onAdClicked,
          onAdOpened: onAdOpened,
          onAdImpression: onAdImpression,
          onAdClosed: onAdClosed,
          onLeftApplication: onLeftApplication,
          onAdFailedToLoad: onAdFailedToLoad,
          onAdEvent: onAdEvent,
        ) {
    _blockAdsUntil = blockAdsUntil;
  }

  /// returns:
  /// ```
  /// AdType.HEALTHY_INTERSTITIAL
  /// ```
  @override
  AdType get adType => AdType.HEALTHY_INTERSTITIAL;

  @override
  void _initValues() {
    super._initValues();
    SP.execute(_restoreValues);
  }

  void _saveValues() {
    //_blockAdsUntilCount
    SP.setInt(_blockAdsUntilCountKey, _blockAdsUntilCount);

    //_count
    SP.setInt(_countKey, _count);

    //_countAffected
    SP.setInt(_countAffectedKey, _countAffected);
  }

  void _restoreValues() {
    //_blockAdsUntilCount
    _blockAdsUntilCount = SP.getInt(_blockAdsUntilCountKey) ?? 0;

    //_count
    _count = SP.getInt(_countKey) ?? 0;

    //_countAffected
    _countAffected = SP.getInt(_countAffectedKey) ?? 0;
  }

  bool _isAdsUnlocked() {
    return (_blockAdsUntil == 0 || _blockAdsUntil - 1 == _blockAdsUntilCount)
        ? true
        : false;
  }

  /// Only shows the InterstitialAd if it has been loaded
  /// and blockAdsUntil has been reached.
  @override
  Future<bool> show({
    double anchorOffset = 0.0,
    double horizontalCenterOffset = 0.0,
    EasyAnchorType anchorType = EasyAnchorType.bottom,
  }) {
    if (_isAdsUnlocked()) {
      return super.show(
        anchorOffset: anchorOffset,
        horizontalCenterOffset: horizontalCenterOffset,
        anchorType: anchorType,
      );
    } else {
      _blockAdsUntilCount++;
      _saveValues();
      return null;
    }
  }

  /// ## Description
  ///
  /// * This `is not` affected by [blockAdsUntil]
  ///
  ///
  /// * The Example assumes, that you have already initialized the
  ///   plugin with [EasyAds.initialize].
  ///
  ///
  /// ## Parameters
  ///
  /// * `onValue`:
  ///
  ///   Amount of times [showOnCount] has to be executed until [show] is called.
  ///
  ///
  /// * `onShowAd`:
  ///
  ///   This function gets executed right before the [EasyInterstitialAd.show] function.
  ///   If onShowAd returns `false`, then the ad does not get shown.
  ///
  ///
  /// * `onNotShowAd`:
  ///
  ///   In case the expression that evaluates if an ad will be shown,
  ///   using onValue and a private static counter variable equals false,
  ///   then this function gets executed.
  ///
  ///
  /// * `showAdOnValue`:
  ///
  ///   In case you do not want this function to automatically show an ad
  ///   if the expression that evaluates if an ad will be shown equals true,
  ///   then set this to `false`.
  ///
  ///
  /// * The remaining parameters are the ones used for [show].
  ///
  ///
  /// ## Example
  ///
  /// {@tool sample}
  /// ```dart
  /// class MyAdWidget extends StatelessWidget {
  ///   @override
  ///   Widget build(BuildContext context) {
  ///     return Scaffold(
  ///       body: Container(
  ///         child: Center(
  ///           child: RaisedButton(
  ///             onPressed: () {
  ///               // Because we set onValue: 3,
  ///               // every third button press will show an ad,
  ///               // but before the ad is shown onShowAd
  ///               // will get executed if it is non-null.
  ///               //
  ///               // In case the user pressed the button for the first or
  ///               // second time, onNotShowAd will get executed
  ///               // if non-null.
  ///               EasyAds.easyInterstitialAd.showOnCount(
  ///                 onValue: 3,
  ///                 onShowAd: () {
  ///                   print("now showing ad");
  ///                   return true;
  ///                 },
  ///                 onNotShowAd: () {
  ///                   print("u r lucky, no ad");
  ///                 },
  ///               );
  ///             },
  ///           ),
  ///         ),
  ///       ),
  ///     );
  ///   }
  /// }
  /// ```
  /// {@end-tool}
  void showOnCount({
    int onValue = 5,
    ShowAdCallback onShowAd,
    NotShowAdCallback onNotShowAd,
    bool showAdOnValue = true,
    double anchorOffset = 0.0,
    double horizontalCenterOffset = 0.0,
    EasyAnchorType anchorType = EasyAnchorType.bottom,
  }) {
    if (_count >= onValue) {
      _count = 0;
      _saveValues();
      if (onShowAd != null && !onShowAd()) return;
      if (showAdOnValue)
        super.show(
          anchorOffset: anchorOffset,
          horizontalCenterOffset: horizontalCenterOffset,
          anchorType: anchorType,
        );
    } else {
      _count++;
      _saveValues();
      if (onNotShowAd != null) onNotShowAd();
    }
  }

  /// ## Description
  ///
  /// * This `is` affected by [blockAdsUntil]
  ///
  ///
  /// * The Example assumes, that you have already initialized the
  ///   plugin with [EasyAds.initialize].
  ///
  ///
  /// ## Parameters
  ///
  /// * `onValue`:
  ///
  ///   Amount of times [showOnCountAffected] has to be executed until [show] is called.
  ///
  ///
  /// * `onShowAd`:
  ///
  ///   This function gets executed right before the [EasyInterstitialAd.show] function.
  ///   If onShowAd returns `false`, then the ad does not get shown.
  ///
  ///
  /// * `onNotShowAd`:
  ///
  ///   In case the expression that evaluates if an ad will be shown,
  ///   using onValue and a private static counter variable equals false,
  ///   then this function gets executed.
  ///
  ///
  /// * `showAdOnValue`:
  ///
  ///   In case you do not want this function to automatically show an ad
  ///   if the expression that evaluates if an ad will be shown equals true,
  ///   then set this to `false`.
  ///
  ///
  /// * The remaining parameters are the ones used for [show].
  ///
  ///
  /// ## Example
  ///
  /// {@tool sample}
  /// ```dart
  /// class MyAdWidget extends StatelessWidget {
  ///   @override
  ///   Widget build(BuildContext context) {
  ///     return Scaffold(
  ///       body: Container(
  ///         child: Center(
  ///           child: RaisedButton(
  ///             onPressed: () {
  ///               // Because we set onValue: 3,
  ///               // every third button press will show an ad,
  ///               // but before the ad is shown onShowAd
  ///               // will get executed if it is non-null.
  ///               //
  ///               // In case the user pressed the button for the first or
  ///               // second time, onNotShowAd will get executed
  ///               // if non-null.
  ///               EasyAds.easyInterstitialAd.showOnCount(
  ///                 onValue: 3,
  ///                 onShowAd: () {
  ///                   print("now showing ad");
  ///                   return true;
  ///                 },
  ///                 onNotShowAd: () {
  ///                   print("u r lucky, no ad");
  ///                 },
  ///               );
  ///             },
  ///           ),
  ///         ),
  ///       ),
  ///     );
  ///   }
  /// }
  /// ```
  /// {@end-tool}
  void showOnCountAffected({
    int onValue = 5,
    ShowAdCallback onShowAd,
    NotShowAdCallback onNotShowAd,
    bool showAdOnValue = true,
    double anchorOffset = 0.0,
    double horizontalCenterOffset = 0.0,
    EasyAnchorType anchorType = EasyAnchorType.bottom,
  }) {
    if (_countAffected >= onValue) {
      _countAffected = 0;
      _saveValues();
      if (onShowAd != null && !onShowAd()) return;
      if (showAdOnValue)
        show(
          anchorOffset: anchorOffset,
          horizontalCenterOffset: horizontalCenterOffset,
          anchorType: anchorType,
        );
    } else {
      _countAffected++;
      _saveValues();
      if (onNotShowAd != null) onNotShowAd();
    }
  }
}
