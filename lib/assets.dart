import 'dart:async';

import 'package:firebase_admob/firebase_admob.dart';

/// [EasyAdSize] represents the size of a banner ad. There are six sizes available,
/// which are the same for both iOS and Android. See the guides for banners on
/// [Android](https://developers.google.com/admob/android/banner#banner_sizes)
/// and [iOS](https://developers.google.com/admob/ios/banner#banner_sizes) for
/// additional details.
enum EasyAdSize {
  banner,
  fullBanner,
  largeBanner,
  leaderboard,
  mediumRectangle,
  smartBanner,
}

/// This method converts [EasyAdSize] to [AdSize] from the
/// firebase_admob package
AdSize getAdSize(EasyAdSize adSize) {
  switch (adSize) {
    case EasyAdSize.banner:
      return AdSize.banner;

    case EasyAdSize.fullBanner:
      return AdSize.fullBanner;

    case EasyAdSize.largeBanner:
      return AdSize.largeBanner;

    case EasyAdSize.leaderboard:
      return AdSize.leaderboard;

    case EasyAdSize.mediumRectangle:
      return AdSize.mediumRectangle;

    case EasyAdSize.smartBanner:
      return AdSize.smartBanner;
  }
  return AdSize.smartBanner;
}

enum EasyAnchorType { bottom, top }

/// This converts [EasyAnchorType] to [AnchorType] from the
/// firebase_admob package.
AnchorType getAnchorType(EasyAnchorType anchorType) =>
    (anchorType == EasyAnchorType.top) ? AnchorType.top : AnchorType.bottom;

enum AdType {
  VIDEO,
  HEALTHY_VIDEO,
  BANNER,
  HEALTHY_BANNER,
  INTERSTITIAL,
  HEALTHY_INTERSTITIAL,
}

enum EasyRewardedVideoAdEvent {
  loaded,
  opened,
  started,
  rewarded,
  completed,
  closed,
  leftApplication,
  failedToLoad,
}

typedef VideoAdEventTriggeredFunction = void Function(
  EasyRewardedVideoAdEvent event, {
  String rewardType,
  int rewardAmount,
});

/// This method converts [RewardedVideoAdEvent] from the firebase_admob package
/// into a [EasyRewardedVideoAdEvent].
EasyRewardedVideoAdEvent getEasyRewardedVideoAdEvent(
    RewardedVideoAdEvent event) {
  switch (event) {
    case RewardedVideoAdEvent.loaded:
      return EasyRewardedVideoAdEvent.loaded;

    case RewardedVideoAdEvent.opened:
      return EasyRewardedVideoAdEvent.opened;

    case RewardedVideoAdEvent.started:
      return EasyRewardedVideoAdEvent.started;

    case RewardedVideoAdEvent.rewarded:
      return EasyRewardedVideoAdEvent.rewarded;

    case RewardedVideoAdEvent.completed:
      return EasyRewardedVideoAdEvent.completed;

    case RewardedVideoAdEvent.closed:
      return EasyRewardedVideoAdEvent.closed;

    case RewardedVideoAdEvent.leftApplication:
      return EasyRewardedVideoAdEvent.leftApplication;

    case RewardedVideoAdEvent.failedToLoad:
      return EasyRewardedVideoAdEvent.failedToLoad;
  }
}

enum EasyMobileAdEvent {
  loaded,
  failedToLoad,
  clicked,
  impression,
  opened,
  leftApplication,
  closed,
}

typedef MobileAdEventTriggeredFunction = void Function(EasyMobileAdEvent event);

/// This method converts [RewardedVideoAdEvent] from the firebase_admob package
/// into a [EasyRewardedVideoAdEvent].
EasyMobileAdEvent getEasyMobileAdEvent(MobileAdEvent event) {
  switch (event) {
    case MobileAdEvent.loaded:
      return EasyMobileAdEvent.loaded;

    case MobileAdEvent.clicked:
      return EasyMobileAdEvent.clicked;

    case MobileAdEvent.opened:
      return EasyMobileAdEvent.opened;

    case MobileAdEvent.impression:
      return EasyMobileAdEvent.impression;

    case MobileAdEvent.closed:
      return EasyMobileAdEvent.closed;

    case MobileAdEvent.leftApplication:
      return EasyMobileAdEvent.leftApplication;

    case MobileAdEvent.failedToLoad:
      return EasyMobileAdEvent.failedToLoad;
  }
}

typedef AdLoadedFunction = void Function();
typedef AdClickedFunction = void Function();
typedef AdImpressionFunction = void Function();
typedef AdOpenedFunction = void Function();
typedef AdStartedFunction = void Function();
typedef AdRewardedFunction = void Function(
    int rewardedAmount, String rewardedType);
typedef AdClosedFunction = void Function();
typedef AdLeftApplicationFunction = void Function();
typedef AdFailedToLoadFunction = void Function();
typedef AdCompletedFunction = void Function();

/// If this callback returns `false`, then the ad will not be shown.
typedef ShowAdCallback = FutureOr<bool> Function();

typedef NotShowAdCallback = FutureOr<void> Function();
