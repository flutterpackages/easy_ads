# easy_ads

A package which gives the developer a static and very simple way to implement Ads.
This package depends on flutter.dev's firebase_admob plugin and on the 
static_sharedpreferences package.

## Getting Started on Android

You have to modifiy the AndroidManifest.xml in the following way for this to work:

### 1)

    Add the following lines in the <application/> tag, infront of the <activity/> tag:


```xml
    <!-- TestAd AdMob App ID: ca-app-pub-3940256099942544~3347511713 -->
    <meta-data
        android:name="com.google.android.gms.ads.APPLICATION_ID"
        android:value="[ADMOB_APP_ID]"/>
```

Replace `[ADMOB_APP_ID]` with your App ID. You must pass the same value when you initialize the plugin in your Dart code.

### 2) This is optional

    Add the following lines in the <manifest/> tag:

```xml
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
```



See https://goo.gl/fQ2neu for more information about configuring AndroidManifest.xml and setting up your App ID.

## Getting Started on IOS

Admob 7.42.0 requires the App ID to be included in `Info.plist`. Failure to do so will result in a crash on launch of your app. The lines should look like:

```xml
    <!-- TestAd AdMob App ID: ca-app-pub-3940256099942544~1458002511 -->
    <key>GADApplicationIdentifier</key>
    <string>[ADMOB_APP_ID]</string>
```

Replace `[ADMOB_APP_ID]` with your App ID. You must pass the same value when you initialize the plugin in your Dart code.



See https://developers.google.com/admob/ios/quick-start#update_your_infoplist for more information about configuring `Info.plist` and setting up your App ID.


## More Info

https://pub.dev/packages/firebase_admob