import 'package:easy_ads/banner_ad.dart';
import 'package:easy_ads/easy_ads.dart';
import 'package:easy_ads/interstital_ad.dart';
import 'package:easy_ads/rewarded_video_ad.dart';
import 'package:flutter/widgets.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  EasyAds.initEasyRewardedVideoAd(
    EasyRewardedVideoAd(
      // adUnitId: rewardVideoAdUnitId,
      onAdLoaded: () {
        print("---------------------Ad Loaded-----------");
      },
      onAdFailedToLoad: () {
        print("--------------------- onAdFailedToLoad-----------");
      },
    ),
  );

  EasyAds.initEasyInterstitialAd(EasyInterstitialAd(
      // adUnitId: interstitialAdUnitId,
      ));

  EasyAds.initEasyBannerAd(
    EasyBannerAd(
      adSize: EasyAdSize.banner,
      // adUnitId: bannerAdUnitId,
      onAdFailedToLoad: () {
        EasyAds.loadEasyBannerAd();
      },
      onLeftApplication: () {
        EasyAds.loadEasyBannerAd();
      },
    ),
  );
}
