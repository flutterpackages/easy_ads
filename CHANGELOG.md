## 2.5.6

- Changed return type of [ShowAdCallback] to `FutureOr<bool>` and [NotShowAdCallback]
  to `FutureOr<void>`.

## 2.5.5

- Added [ShowAdCallback] and [NotShowAdCallback] and implemented those two in [showOnCount]
  and [showOnCountAffected] for every HealthyAd. [ShowAdCallback] gives the option to return
  a bool which decides if an ad should be shown or not.

## 2.5.4

- Fixed a potential bug where [showAdOnCount] and [showAdOnCountAffected] would stop working.

## 2.5.3

- Added 'isRewarded' to EasyRewardedVideoAd.

## 2.5.2

- Added: export 'assets.dart'; to easy_ads.dart
- Added: export 'mobile_ad_targeting_info.dart'; to easy_ads.dart
- Changed a few docs.

## 2.5.1

- Updated dependencies.

## 2.5.0

- Added a Timer on AdFailedToLoad, so that if [autoReload] is enabled
  the ad does not execute [load] in kind of a 'while(true)' loop

## 2.4.1

- Fixed a bug where MobileAdTargetingInfo was null in the EasyRewardedVideoAd.

## 2.4.0

- Fixed onAdEvent Function. Removed the necesserity to depend on the firebase_admob package
  for some enums and classes (RewardedVideoAdEvent, MobileAdEvent, AdSize, AnchorType,...)

## 2.3.0

- Fixed some bugs in banner_ad.dart and interstitial_ad.dart and changed some docs.

## 2.2.1

- Fixed bug concerning _count in rewarded_video_ad.dart

## 2.2.0

- Fixed some bugs concerning showOnCount and showOnCountAffected

## 2.1.3

- Changed some docs.

## 2.1.2

- Changed some docs.

## 2.1.1

- Fixed the bug where blockAdsUntil in HealthyEasyRewardedVideoAd was not initialized.

## 2.1.0

- Added the ability to block ads for a certain amount of events. Has not been checked for bugs yet.

## 2.0.0

- Added healty ads classes.

## 1.3.2

- Updated dependencies.

## 1.3.1

- Updated dependencies.

## 1.3.0

- Finally fixed the bug which caused HealthyEasyRewardedVideoAd to not work in its' intended way.

* Also updated some dependencies.

## 1.2.3

- Minor bugfix.

## 1.2.2

- Minor bugfix.

## 1.2.1

- Fixed a bug where SharedPreferences took woo long to initialize.

## 1.2.0

- Should have fixed HealthyEasyRewardedVideoAd now.

## 1.1.0

- Added HealthyEasyRewardedVideoAd class.

## 1.0.0

- First release
